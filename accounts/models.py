from django.db import models


class EventModel(models.Model):
    """docstring for EventModel."""

    event_name = models.CharField(max_length=250)
    event_date = models.DateField()

    def __str__(self):
        return self.event_name


class GalleryModel(models.Model):
    """docstring for GalleryModel."""
    image = models.ImageField(upload_to="gallery")
    event_category = models.ForeignKey(EventModel,on_delete=models.CASCADE,blank=True)
    description = models.CharField(max_length=250)
    created_at = models.DateField(auto_now=True)
    created_by = models.ForeignKey('auth.User',on_delete=models.CASCADE,blank=True)
    def __str__(self):
        return self.image.name

class GroupModel(models.Model):
    """docstring for GroupModel."""
    group_name = models.CharField(max_length=250)
    group_logo = models.ImageField(upload_to="group", blank=True)
    group_point = models.IntegerField()
    def __str__(self):
        return self.group_name

class WinnersModel(models.Model):
    """docstring for WinnersModel."""
    FIRST = 'FIRST PLACE'
    SECOND = 'SECOND PLACE'
    THIRD = 'THIRD PLACE'
    WINNER = [
        (FIRST, 'First'),
        (SECOND, 'Second'),
        (THIRD, 'Third')
        ]
    winner_place = models.CharField(
        max_length=20,
        choices=WINNER,
        default=FIRST,
    )
    winner_name = models.CharField(max_length=150)
    winner_image = models.ImageField(upload_to="winner", blank=True)
    event_category = models.ForeignKey(EventModel,on_delete=models.CASCADE,blank=True)
    group_category = models.ForeignKey(GroupModel,on_delete=models.CASCADE,blank=True)
    def __str__(self):
        return "{winner_name} - {winner_place} -{event_name}".format(winner_name=self.winner_name,winner_place=self.winner_place,event_name=self.event_category.event_name)
