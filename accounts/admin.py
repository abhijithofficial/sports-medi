from django.contrib import admin
from . import models

admin.site.register(models.EventModel)
admin.site.register(models.GalleryModel)
admin.site.register(models.GroupModel)
admin.site.register(models.WinnersModel)
