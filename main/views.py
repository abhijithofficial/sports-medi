from django.shortcuts import render
from accounts import models

def home_page(request):
	return render(request,'main/index.html')

def gallery(request):
	gallery = models.GalleryModel.objects.all()
	return render(request,'main/gallery.html',{'gallery':gallery})

def point_table(request):
	point_table = models.GroupModel.objects.all().order_by('-group_point')
	return render(request,'main/point-table.html',{'point_table':point_table})

def events(request):
	events = models.EventModel.objects.all()
	return render(request,'main/events.html',{'events':events})

def winners(request,event_id):
	winners_object = models.WinnersModel.objects.filter(event_category__id=event_id)
	return render(request,'main/winner.html',{'winners':winners_object})

def main_page(request):
	return render(request,'main/home.html')
