from django.urls import path
from . import views

urlpatterns = [
    path('',views.home_page),
    path('home/',views.main_page,name="home_page"),
    path('gallery/',views.gallery,name="gallery"),
    path('point_table/',views.point_table,name="point_table"),
    path('events/',views.events,name="events"),
    path('events/<int:event_id>/',views.winners,name="winners"),
]
